import { Component, OnInit } from '@angular/core';
import * as AOS from 'aos';
import { Title, Meta } from '@angular/platform-browser';
import { LanguageService } from "src/app/services/language/language.service"

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'muffeez-portfolio';

  constructor(
    private titleService: Title,
    private metaService: Meta,
    private languageService: LanguageService
  ) {
  }
  ngOnInit(): void {

    this.languageService.initLanguage()

    this.titleService.setTitle("Yara Hmoud | Frontend Developer | Angular Development | Software Engineer");
    this.metaService.addTags([
      { name: 'keywords', content: 'Frontend, Angular Developer , React Development , Software Engineer, software, developer' },
      { name: 'description', content: 'As a software engineer with 4+ years of hands-on experience in developing scalable web applications using a wide range of frontend and backend technologies like React, Angular, Laravel and Cakephp.' },
    ]);
    
    AOS.init();


  }
}
